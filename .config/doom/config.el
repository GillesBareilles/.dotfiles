;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!


;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets. It is optional.
(setq user-full-name "Gilles Bareilles"
      user-mail-address "gilles.bareilles@protonmail.com")

;; Doom exposes five (optional) variables for controlling fonts in Doom:
;;
;; - `doom-font' -- the primary font to use
;; - `doom-variable-pitch-font' -- a non-monospace font (where applicable)
;; - `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;; - `doom-unicode-font' -- for unicode glyphs
;; - `doom-serif-font' -- for the `fixed-pitch-serif' face
;;
;; See 'C-h v doom-font' for documentation and more examples of what they
;; accept. For example:
;;
(setq doom-font (font-spec :family "Fira Code" :size 16 :weight 'regular)
    doom-variable-pitch-font (font-spec :family "Fira Sans Light" :size 13))
;;
;; If you or Emacs can't find your font, use 'M-x describe-font' to look them
;; up, `M-x eval-region' to execute elisp code, and 'M-x doom/reload-font' to
;; refresh your font settings. If Emacs still can't find your font, it likely
;; wasn't installed correctly. Font issues are rarely Doom issues!

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:
(setq doom-theme 'doom-one)
;; (setq doom-theme 'doom-dracula)

;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
(setq display-line-numbers-type nil)

;; If you use `org' and don't want your org files in the default location below,
;; change `org-directory'. It must be set before org loads!
;; See in the org section

;; Whenever you reconfigure a package, make sure to wrap your config in an
;; `after!' block, otherwise Doom's defaults may override your settings. E.g.
;;
;;   (after! PACKAGE
;;     (setq x y))
;;
;; The exceptions to this rule:
;;
;;   - Setting file/directory variables (like `org-directory')
;;   - Setting variables which explicitly tell you to set them before their
;;     package is loaded (see 'C-h v VARIABLE' to look up their documentation).
;;   - Setting doom variables (which start with 'doom-' or '+').
;;
;; Here are some additional functions/macros that will help you configure Doom.
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package!' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c c k').
;; This will open documentation for it, including demos of how they are used.
;; Alternatively, use `C-h o' to look up a symbol (functions, variables, faces,
;; etc).
;;
;; You can also try 'gd' (or 'C-c c d') to jump to their definition and see how
;; they are implemented.


(add-hook 'emacs-startup-hook 'global-jinx-mode)

(setq calendar-week-start-day 1)
(add-hook 'emacs-startup-hook 'toggle-frame-fullscreen)
(delete-selection-mode 1) ; Replace selection when inserting text
(display-time-mode 1)     ; Enable time in the mode-line

;; Evil trainer
(use-package! evil-motion-trainer
  :config
  ;; (setq evil-motion-trainer-super-annoying-mode t)
  (setq global-evil-motion-trainer-mode 1))

(use-package! org-roam
  :after org
  :config
  (setq org-roam-directory "/home/gilles/org/RoamNotes")
  )

;; Org roam config
(use-package! websocket
  :after org-roam)

(use-package! org-roam-ui
  :after org-roam ;; or :after org
  ;;         normally we'd recommend hooking orui after org-roam, but since org-roam does not have
  ;;         a hookable mode anymore, you're advised to pick something yourself
  ;;         if you don't care about startup time, use
  ;;  :hook (after-init . org-roam-ui-mode)
  :config
  (setq org-roam-ui-sync-theme t
        org-roam-ui-follow t
        org-roam-ui-update-on-save t
        org-roam-ui-open-on-start t))



(setq projectile-project-search-path '("~/.julia/dev" "~/org"))
(setq avy-keys '(?a ?o ?e ?u ?i ?d ?h ?t ?n))
;; (add-to-list 'exec-path "/home/gilles/Documents/julia/bin")

(after! julia-mode
  (add-hook! 'julia-mode-hook
    (setq-local lsp-enable-folding t
                lsp-folding-range-limit 100)))

;; (setq julia-repl-executable-records
;;       '((default "/home/gilles/Documents/julia/bin/julia")
;;         (master "~/src/julia-git/julia"))) ; compiled from the repository

(setq evil-escape-key-sequence "ht")
(setq doom-themes-treemacs-theme "doom-colors")

;; citations
(setq citar-bibliography '("/home/gilles/Zotero/bibtex/biblio.bib")
      citar-library-paths '("/home/gilles/Zotero/storage")
      citar-notes-paths '("/home/gilles/org/RoamNotes"))

(use-package! org-roam-bibtex
  :after org-roam)

(add-hook 'pdf-view-mode-hook
          (lambda ()
            (pdf-view-auto-slice-minor-mode 1)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; LaTeX
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; NOTE LaTeX
;; Sioyek integration
;; (setq TeX-view-program-list
;;       '(("Sioyek"
;;         ("sioyek %o --reuse-instance"
;;          (mode-io-correlate " --forward-search-file %b --forward-search-line %n --inverse-search \"emacsclient --no-wait +%2:%3 %1\"")))))
(setq TeX-view-program-selection
      '(((output-dvi has-no-display-manager)
         "dvi2tty")
        ((output-dvi style-pstricks)
         "dvips and gv")
        (output-dvi "xdvi")
        (output-pdf "Sioyek")
        (output-html "xdg-open")))
      ;; +latex-viewers '(sioyek))

(setq TeX-view-program-list
      '(("Okular"
         ("okular --noraise --unique file:%o"
          (mode-io-correlate "#src:%n%a")))
        ("preview-pane" latex-preview-pane-mode)
        ("Sioyek"
         ("sioyek %o --reuse-instance"
          (mode-io-correlate " --forward-search-file %b --forward-search-line %n --inverse-search \"emacsclient --no-wait +%2:%3 %1\"")))))

(add-hook 'LaTeX-mode-hook #'TeX-source-correlate-mode)

(with-eval-after-load 'latex
  (define-key TeX-source-correlate-map [C-down-mouse-1] #'TeX-view-mouse))

(setq
 +latex-viewers '(sioyek pdf-tools skim evince sumatrapdf zathura okular)
 TeX-command-extra-options "--shell-escape"
 +latex-indent-item-continuation-offset 'auto
 )

(add-hook 'TeX-update-style-hook 'hl-todo-mode) ;; Add todo highlight to latex

(map! :after latex
      :localleader
      :map latex-mode-map
      :desc "insert citation" "i" #'citar-insert-citation
      :map LaTeX-mode-map
      :desc "insert citation" "i" #'citar-insert-citation)

;; NOTE Dirvish
(setq dirvish-hide-details t)


;; (use-package! julia-vterm)
;; (add-to-list 'org-babel-load-languages '(julia-vterm . t))
;; (org-babel-do-load-languages 'org-babel-load-languages org-babel-load-languages)


;; NOTE Corfu
;; HACK: make lsp and corfu behave, copied from https://github.com/doomemacs/doomemacs/compare/master...LuigiPiucco:doom-emacs:master
(setq lsp-completion-provider :none)
(add-hook 'lsp-mode-hook #'lsp-completion-mode)



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; NOTE Org
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(require 'org)
(require 'org-habit)

(map! :map org-mode-map
      "M-n" #'outline-next-visible-heading
      "M-p" #'outline-previous-visible-heading)

;; (setq org-tag-alist '((:startgrouptag)
;;                       (:grouptags)
;;                       ("@home" . ?h)
;;                       ("@computer")
;;                       ("@work")
;;                       ("@labor")
;;                       ("@read")
;;                       ("@roam")
;;                       ("@math")
;;                       ("@writing")
;;                       ("@meeting")
;;                       ("@programming")
;;                       ;; ("@brainstorm")
;;                       ("@planning")
;;                       ("SOMEDAY")
;;                       ("WAITING")
;;                       (:endgrouptag)))

(setq org-todo-keywords
      '((sequence "TODO(t)" "NEXT(n)" "HOLD(h)" "|" "DONE(d)" "|" "Cancelled(c)")))

(setq org-tag-alist '(("@maison" . ?m) ;; TODO review this
                      ("@travail" . ?t)
                      ("@course" . ?c)
                      ("@kolkol" . ?k)
                      ("reading" . ?r)
                      ))



(after! org
  (setq org-directory "~/org/"
        org-ellipsis " ▼ ")
  ;; (setq org-todo-keywords
  ;;       '((sequence
  ;;          "TODO(t!)"  ; A task that needs doing & is ready to do
  ;;          "PROJ(p)"  ; A project, which usually contains other tasks
  ;;          ;; "LOOP(r)"  ; A recurring task
  ;;          "STRT(s!)"  ; A task that is in progress
  ;;          "WAIT(w)"  ; Something external is holding up this task
  ;;          "HOLD(h)"  ; This task is paused/on hold because of me
  ;;          "IDEA(i)"  ; An unconfirmed and unapproved task or notion
  ;;          "|"
  ;;          "DONE(d!)"  ; Task successfully completed
  ;;          "KILL(k!)") ; Task was cancelled, aborted or is no longer applicable
  ;;         (sequence
  ;;          "[ ](T)"   ; A task that needs doing
  ;;          "[-](S)"   ; Task is in progress
  ;;          "[?](W)"   ; Task is being held up or paused
  ;;          "|"
  ;;          "[X](D)")  ; Task was completed
  ;;         ;; (sequence
  ;;         ;;  "|"
  ;;         ;;  "OKAY(o)"
  ;;         ;;  "YES(y)"
  ;;         ;;  "NO(n)"))
  ;;         ))
  (setq
        org-todo-keyword-faces
        '(("[-]"  . +org-todo-active)
          ("STRT" . +org-todo-active)
          ("TODO" . +org-todo-active)
          ("[?]"  . +org-todo-onhold)
          ("WAIT" . +org-todo-onhold)
          ("HOLD" . +org-todo-onhold)
          ("PROJ" . +org-todo-project)
          ;; ("NO"   . +org-todo-cancel)
          ("NEXT" . +org-todo-next)
          ("KILL" . +org-todo-cancel))
        org-agenda-include-diary t
        )
  ;; (setq org-log-into-drawer t)
  ;; (setq +org-capture-notes-file "in-tray.org")
  ;; (setq +org-capture-todo-file "in-tray.org")
  )


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; NOTE Org agenda
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(require 'find-lisp)

(setq jethro/org-agenda-directory
      (expand-file-name "gtd/" org-directory))
(setq org-agenda-files
      (find-lisp-find-files jethro/org-agenda-directory "\.org$"))

; NOTE: overides doom's templates
(setq org-capture-templates
      `(("i" "Inbox" entry
         (file "gtd/inbox.org")
         "* TODO %?\n%i\n%a\n/Entered on/ %U" :prepend t)
        ;; ("s" "Slipbox" entry  (file "braindump/org/inbox.org")
        ;;  "* %?\n")
        )
      )

;; Doom's default capture system
;; (("t" "Personal todo" entry
;;   (file+headline +org-capture-todo-file "Inbox")
;;   "* [ ] %?\n%i\n%a" :prepend t)
;;  ("n" "Personal notes" entry
;;   (file+headline +org-capture-notes-file "Inbox")
;;   "* %u %?\n%i\n%a" :prepend t)
;;  ("j" "Journal" entry
;;   (file+olp+datetree +org-capture-journal-file)
;;   "* %U %?\n%i\n%a" :prepend t)
;;  ("p" "Templates for projects")
;;  ("pt" "Project-local todo" entry
;;   (file+headline +org-capture-project-todo-file "Inbox")
;;   "* TODO %?\n%i\n%a" :prepend t)
;;  ("pn" "Project-local notes" entry
;;   (file+headline +org-capture-project-notes-file "Inbox")
;;   "* %U %?\n%i\n%a" :prepend t)
;;  ("pc" "Project-local changelog" entry
;;   (file+headline +org-capture-project-changelog-file "Unreleased")
;;   "* %U %?\n%i\n%a" :prepend t)
;;  ("o" "Centralized templates for projects")
;;  ("ot" "Project todo" entry #'+org-capture-central-project-todo-file "* TODO %?\n %i\n %a" :heading "Tasks" :prepend nil)
;;  ("on" "Project notes" entry #'+org-capture-central-project-notes-file "* %U %?\n %i\n %a" :heading "Notes" :prepend t)
;;  ("oc" "Project changelog" entry #'+org-capture-central-project-changelog-file "* %U %?\n %i\n %a" :heading "Changelog" :prepend t))



(defun jethro/org-capture-inbox ()
  (interactive)
  (org-capture nil "i"))

;; (defun jethro/org-capture-slipbox ()
;;   (interactive)
;;   (org-capture nil "s"))

;; (defun jethro/org-agenda ()
;;   (interactive)
;;   (org-agenda nil " "))

(bind-key "C-c <tab>" #'jethro/org-capture-inbox)
;; (bind-key "C-c SPC" #'jethro/org-agenda)

; NOTE ignored for now
(defun log-todo-next-creation-date (&rest ignore)
  "Log NEXT creation time in the property drawer under the key 'ACTIVATED'"
  (when (and (string= (org-get-todo-state) "NEXT")
             (not (org-entry-get nil "ACTIVATED")))
    (org-entry-put nil "ACTIVATED" (format-time-string "[%Y-%m-%d]"))))
(add-hook 'org-after-todo-state-change-hook #'log-todo-next-creation-date)

(setq org-log-done 'time
      org-log-into-drawer t
      org-log-state-notes-insert-after-drawers nil)

(setq org-fast-tag-selection-single-key nil)
(setq org-refile-use-outline-path 'file
      org-outline-path-complete-in-steps nil)
(setq org-refile-allow-creating-parent-nodes 'confirm
      org-refile-targets '(("projects.org" . (:level . 1))))



;;;; Effort
(defvar jethro/org-current-effort "1:00"
  "Current effort for agenda items.")

(defun jethro/my-org-agenda-set-effort (effort)
  "Set the effort property for the current headline."
  (interactive
   (list (read-string (format "Effort [%s]: " jethro/org-current-effort) nil nil jethro/org-current-effort)))
  (setq jethro/org-current-effort effort)
  (org-agenda-check-no-diary)
  (let* ((hdmarker (or (org-get-at-bol 'org-hd-marker)
                       (org-agenda-error)))
         (buffer (marker-buffer hdmarker))
         (pos (marker-position hdmarker))
         (inhibit-read-only t)
         newhead)
    (org-with-remote-undo buffer
      (with-current-buffer buffer
        (widen)
        (goto-char pos)
        (org-show-context 'agenda)
        (funcall-interactively 'org-set-effort nil jethro/org-current-effort)
        (end-of-line 1)
        (setq newhead (org-get-heading)))
      (org-agenda-change-all-lines newhead hdmarker))))

;;;; Process one inbox item
(defun jethro/org-agenda-process-inbox-item ()
  "Process a single item in the org-agenda."
  (interactive
  (org-with-wide-buffer
   (org-agenda-set-tags)
   (org-agenda-priority)
   (call-interactively 'jethro/my-org-agenda-set-effort)
   (org-agenda-refile nil nil))))

(map! "<f2>" #'jethro/org-agenda-process-inbox-item)


;;;; Process all inbox items
;; TODO understand this.
(defun jethro/bulk-process-entries ()
  (let ())
  (if (not (null org-agenda-bulk-marked-entries))
      (let ((entries (reverse org-agenda-bulk-marked-entries))
            (processed 0)
            (skipped 0))
        (dolist (e entries)
          (let ((pos (text-property-any (point-min) (point-max) 'org-hd-marker e)))
            (if (not pos)
                (progn (message "Skipping removed entry at %s" e)
                       (cl-incf skipped))
              (goto-char pos)
              (let (org-loop-over-headlines-in-active-region) (funcall 'jethro/org-agenda-process-inbox-item))
              ;; `post-command-hook' is not run yet.  We make sure any
              ;; pending log note is processed.
              (when (or (memq 'org-add-log-note (default-value 'post-command-hook))
                        (memq 'org-add-log-note post-command-hook))
                (org-add-log-note))
              (cl-incf processed))))
        (org-agenda-redo)
        (unless org-agenda-persistent-marks (org-agenda-bulk-unmark-all))
        (message "Acted on %d entries%s%s"
                 processed
                 (if (= skipped 0)
                     ""
                   (format ", skipped %d (disappeared before their turn)"
                           skipped))
                 (if (not org-agenda-persistent-marks) "" " (kept marked)")))))


;; TODO understand this.
;; BUG
(defun jethro/org-process-inbox ()
  "Called in org-agenda-mode, processes all inbox items."
  (interactive)
  (org-agenda-bulk-mark-regexp "inbox:")
  (jethro/bulk-process-entries))

(defun jethro/org-inbox-capture ()
  (interactive)
  "Capture a task in agenda mode."
  (org-capture nil "i"))


; TODO how to access this?
(map! :map org-agenda-mode-map
      "i" #'org-agenda-clock-in
      "I" #'jethro/clock-in-and-advance
      "r" #'jethro/org-process-inbox
      "R" #'org-agenda-refile
      "c" #'jethro/org-inbox-capture)


;;;; Clocking and task status update
;; TODO understand this.
(defun jethro/advance-todo ()
  (org-todo 'right)
  (remove-hook 'org-clock-in-hook #'jethro/advance-todo))

(defun jethro/clock-in-and-advance ()
  (interactive)
  (add-hook 'org-clock-in-hook 'jethro/advance-todo)
  (org-agenda-clock-in))

(use-package! org-clock-convenience
  :bind (:map org-agenda-mode-map
         ("<S-up>" . org-clock-convenience-timestamp-up)
         ("<S-down>" . org-clock-convenience-timestamp-down)
         ("o" . org-clock-convenience-fill-gap)
         ("e" . org-clock-convenience-fill-gap-both)))


;;;; Org agenda main setup
(use-package! org-agenda
  :init
  (map! "<f1>" #'jethro/switch-to-agenda)
  (setq org-agenda-block-separator nil
        org-agenda-start-with-log-mode t
        org-agenda-window-setup 'other-window
        org-agenda-restore-windows-after-quit t)
  (defun jethro/switch-to-agenda ()
    (interactive)
    (org-agenda nil " "))
  :config
  ;; (defun jethro/is-project-p () ;; NOTE: unused
  ;;   "Any task with a todo keyword subtask"
  ;;   (save-restriction
  ;;     (widen)
  ;;     (let ((has-subtask)
  ;;           (subtree-end (save-excursion (org-end-of-subtree t)))
  ;;           (is-a-task (member (nth 2 (org-heading-components)) org-todo-keywords-1)))
  ;;       (save-excursion
  ;;         (forward-line 1)
  ;;         (while (and (not has-subtask)
  ;;                     (< (point) subtree-end)
  ;;                     (re-search-forward "^\*+ " subtree-end t))
  ;;           (when (member (org-get-todo-state) org-todo-keywords-1)
  ;;             (setq has-subtask t))))
  ;;       (and is-a-task has-subtask))))
  (defun jethro/skip-projects ()
    "Skip trees that are projects."
    (save-restriction
      (widen)
      (let ((next-headline (save-excursion (or (outline-next-heading) (point-max)))))
        (cond
         ((org-is-habit-p)
          next-headline)
         (t
          nil)))))
  (setq org-columns-default-format "%40ITEM(Task) %Effort(EE){:} %CLOCKSUM(Time Spent) %SCHEDULED(Scheduled) %DEADLINE(Deadline)") ;;;; NOTE ?
  (setq org-agenda-custom-commands `((" " "Agenda"
                                      ((alltodo ""
                                                ((org-agenda-overriding-header "Inbox")
                                                 (org-agenda-files `(,(expand-file-name "gtd/inbox.org" org-directory)))))
                                       (agenda ""
                                               ((org-agenda-span 'week)
                                                (org-deadline-warning-days 365)))
                                       (todo "NEXT"
                                             ((org-agenda-overriding-header "In Progress")
                                              (org-agenda-files `(,(expand-file-name "gtd/projects.org" org-directory)))))
                                       ;; (stuck ""
                                       ;;       ((org-agenda-overriding-header "stuck")
                                       ;;        (org-agenda-files `(,(expand-file-name "gtd/projects.org" org-directory)))))
                                       (todo "TODO"
                                             ((org-agenda-overriding-header "Active Projects")
                                              (org-agenda-files `(,(expand-file-name "gtd/projects.org" org-directory)))
                                              (org-agenda-skip-function #'jethro/skip-projects)
                                              ))))))
  )


(setq langtool-language-tool-jar "/home/gilles/Documents/LanguageTool-6.2/languagetool-commandline.jar")


;;;;;;;;;;;;;;;;;;;;
;; (setq jethro/org-agenda-todo-view
;;       `(" " "Agenda custom"
;;         ((agenda ""
;;                  ((org-agenda-span 'day)
;;                   (org-deadline-warning-days 365)))
;;          (todo "TODO"
;;                ((org-agenda-overriding-header "To Refile")
;;                 (org-agenda-files '(,(concat jethro/org-agenda-directory "inbox.org")))))
;;          (todo "TODO"
;;                ((org-agenda-overriding-header "Emails")
;;                 (org-agenda-files '(,(concat jethro/org-agenda-directory "emails.org")))))
;;          (todo "NEXT"
;;                ((org-agenda-overriding-header "In Progress")
;;                 (org-agenda-files '(,(concat jethro/org-agenda-directory "someday.org")
;;                                     ,(concat jethro/org-agenda-directory "projects.org")
;;                                     ,(concat jethro/org-agenda-directory "next.org")))
;;                 ))
;;          (todo "TODO"
;;                ((org-agenda-overriding-header "Projects")
;;                 (org-agenda-files '(,(concat jethro/org-agenda-directory "projects.org")))
;;                 ))
;;          (todo "TODO"
;;                ((org-agenda-overriding-header "One-off Tasks")
;;                 (org-agenda-files '(,(concat jethro/org-agenda-directory "next.org")))
;;                 (org-agenda-skip-function '(org-agenda-skip-entry-if 'deadline 'scheduled))))
;;          nil)))
;; (add-to-list 'org-agenda-custom-commands `,jethro/org-agenda-todo-view)


(setq shell-file-name (executable-find "bash"))
(setq-default vterm-shell (executable-find "fish"))
(setq-default explicit-shell-file-name (executable-find "fish"))


;; ;; Mail
;; (after! mu4e
;;   (setq sendmail-program (executable-find "msmtp")
;;         send-mail-function #'smtpmail-send-it
;;         message-sendmail-f-is-evil t
;;         message-sendmail-extra-arguments '("--read-envelope-from")
;;         message-send-mail-function #'message-send-mail-with-sendmail))

;; (set-email-account! "fel - cvut"
;;   '((mu4e-sent-folder       . "/fel/Sent")
;;     (mu4e-drafts-folder     . "/fel/Drafts")
;;     (mu4e-trash-folder      . "/fel/Trash")
;;     (mu4e-refile-folder     . "/fel/All Mail")
;;     (smtpmail-smtp-user     . "gilles.bareilles@fel.cvut.cz")
;;     (mu4e-compose-signature . "---\nBest,\nGilles"))
;;   t)

;; (setq mu4e-update-interval 60)

;; (global-activity-watch-mode)


(setq magit-todos-mode 1)
