# import REPL
# import REPL.LineEdit

using Revise, OhMyREPL

# const mykeys = Dict{Any,Any}(
#     # Up Arrow
#     "\e[A" => (s,o...)->(LineEdit.edit_move_up(s) || LineEdit.history_prev(s, LineEdit.mode(s).hist)),
#     # Down Arrow
#     "\e[B" => (s,o...)->(LineEdit.edit_move_down(s) || LineEdit.history_next(s, LineEdit.mode(s).hist))
# )

# function customize_keys(repl)
#     repl.interface = REPL.setup_interface(repl; extra_repl_keymap = mykeys)
# end

# atreplinit(customize_keys)

ENV["JULIA_PKG_USE_CLI_GIT"] = true # from julia 1.7 up, use git cli rather than the buggy libgit

using DrWatson
using Term

ENV["PATH"] = ENV["PATH"]*":/home/gilles/Documents/alphaCertifiedCode/"
